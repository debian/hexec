/***************************************************************************
 *   Copyright (C) 2008 by Alexander Block                                 *
 *   ablock@blocksoftware.net                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "config.h"

#include "args.h"
#include "error.h"

#include <stdlib.h>
#include <string.h>

static int hexec_args_grow(struct args_t* args, int argc)
{
    if(args->max_argc >= argc)
        return 0;
    args->argv = (char**)realloc(args->argv, (argc + 1) * sizeof(char*));
    args->max_argc = argc;
    return 0;
}

int hexec_args_init(struct args_t* args)
{
    args->argv = NULL;
    args->argc = 0;
    args->max_argc = 0;
    return 0;
}

int hexec_args_free(struct args_t* args)
{
    int i;
    for(i = 0; i < args->argc; i++)
        free(args->argv[i]);
    free(args->argv);
    args->argv = NULL;
    args->argc = 0;
    args->max_argc = 0;
    return 0;
}

int hexec_args_add(struct args_t* args, const char* arg)
{
    hexec_args_grow(args, args->argc + 1);
    args->argv[args->argc++] = strdup(arg);
    args->argv[args->argc] = NULL;
    return 0;
}

int hexec_args_remove(struct args_t* args, int idx)
{
    free(args->argv[idx]);
    memmove(args->argv + idx, args->argv + idx + 1, (args->argc - idx - 1) * sizeof(char*));
    args->argc--;
    args->argv[args->argc] = NULL;
    return 0;
}

int hexec_args_replace(struct args_t* args, int idx, const char* arg)
{
    free(args->argv[idx]);
    args->argv[idx] = strdup(arg);
    return 0;
}

int hexec_args_from_array(struct args_t* args, char* const argv[])
{
    int i;
    hexec_args_init(args);
    for(i = 0; argv[i]; i++)
        hexec_args_add(args, argv[i]);
    return 0;
}

int hexec_args_from_va_list(struct args_t* args, const char* first, va_list* va)
{
    hexec_args_init(args);
    if(first)
        hexec_args_add(args, first);
    while(1)
    {
        const char* arg = va_arg(*va, const char*);
        if(!arg)
            break;
        hexec_args_add(args, arg);
    }
    return 0;
}

int hexec_args_print(struct args_t* args)
{
    int i;
    for(i = 0; i < args->argc; i++)
    {
        if(i)
            hexec_log(" ");
        hexec_log("\"%s\"", args->argv[i]);        
    }
    return 0;
}

