/***************************************************************************
 *   Copyright (C) 2008 by Alexander Block                                 *
 *   ablock@blocksoftware.net                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __HEXEC_ARGS_H__
#define __HEXEC_ARGS_H__

#include <stdarg.h>
#include <stdio.h>

struct args_t
{
    char** argv;
    int argc;

    int max_argc;
};

int hexec_args_init(struct args_t* args);
int hexec_args_free(struct args_t* args);

int hexec_args_add(struct args_t* args, const char* arg);
int hexec_args_remove(struct args_t* args, int idx);
int hexec_args_replace(struct args_t* args, int idx, const char* arg);

int hexec_args_from_array(struct args_t* args, char* const argv[]);
int hexec_args_from_va_list(struct args_t* args, const char* first, va_list* va);

int hexec_args_print(struct args_t* args);

#endif//__HEXEC_ARGS_H__

