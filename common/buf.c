/***************************************************************************
 *   Copyright (C) 2008 by Alexander Block                                 *
 *   ablock@blocksoftware.net                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "config.h"

#include "buf.h"

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <assert.h>

int hexec_buf_create(struct hexec_buf* buf)
{
    buf->owner = true;
    buf->buf = malloc(1024);
    buf->max_size = 1024;
    buf->buf_size = 0;
    return 0;
}

int hexec_buf_create_from(struct hexec_buf* buf, void* from, int size, bool own)
{
    buf->owner = own;
    buf->buf = (char*)from;
    buf->buf_size = size;
    buf->max_size = size;
    return 0;
}

int hexec_buf_destroy(struct hexec_buf* buf)
{
    if(buf->owner)
        free(buf->buf);
    return 0;
}

int hexec_buf_alloc(struct hexec_buf* buf, int size)
{
    assert(buf->owner);

    bool do_realloc = false;
    while(buf->buf_size + size > buf->max_size)
    {
        buf->max_size += 1024;
        do_realloc = true;
    }
    if(do_realloc)
        buf->buf = realloc(buf->buf, buf->max_size);

    int ptr = buf->buf_size;
    buf->buf_size += size;
    return ptr;
}

int hexec_buf_copy_to(struct hexec_buf* buf, void* to, int size)
{
    if(buf->buf_size < size) 
        size = buf->buf_size;
    memcpy(to, buf->buf, size);
    return size;
}

int hexec_buf_strdup(struct hexec_buf* buf, const char* str)
{
    int ptr = hexec_buf_alloc(buf, strlen(str) + 1);
    strcpy(buf->buf + ptr, str);
    return ptr;
}


