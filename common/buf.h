/***************************************************************************
 *   Copyright (C) 2008 by Alexander Block                                 *
 *   ablock@blocksoftware.net                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __HEXEC_BUF_H__
#define __HEXEC_BUF_H__

#include <stdbool.h>

struct hexec_buf
{
    bool owner;
    char* buf;
    int buf_size;
    int max_size;
};

int hexec_buf_create(struct hexec_buf* buf);
int hexec_buf_create_from(struct hexec_buf* buf, void* from, int size, bool own);
int hexec_buf_destroy(struct hexec_buf* buf);

int hexec_buf_alloc(struct hexec_buf* buf, int size);
int hexec_buf_copy_to(struct hexec_buf* buf, void* to, int size);

int hexec_buf_strdup(struct hexec_buf* buf, const char* str);

#endif//__HEXEC_BUF_H__


