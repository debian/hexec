/***************************************************************************
 *   Copyright (C) 2008 by Alexander Block                                 *
 *   ablock@blocksoftware.net                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "config.h"

#include "error.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#include <assert.h>

static char* log_out_path;
int log_out_fd = -1;

void hexec_lock_log()
{
    int err = lockf(log_out_fd, F_LOCK, 0);
    assert(!err);
}

void hexec_unlock_log()
{
    int err = lockf(log_out_fd, F_ULOCK, 0);
    assert(!err);
}

int hexec_open_log_fd(const char* path)
{
    log_out_fd = open(path, O_WRONLY | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
    if(log_out_fd == -1)
        return -1;

    return 0;
}

void hexec_log(const char* str, ...)
{
    if(log_out_fd == -1)
        return;
    int err = errno;
    va_list va;
    va_start(va, str);
    //dprintf(log_out_fd, "hexec: ");
    vdprintf(log_out_fd, str, va);
    va_end(va);
    errno = err;
}

void hexec_warning(const char* str, ...)
{
    if(log_out_fd == -1)
        return;
    int err = errno;
    va_list va;
    va_start(va, str);
    //dprintf(log_out_fd, "hexec: ");
    vdprintf(log_out_fd, str, va);
    va_end(va);
    errno = err;
}

void hexec_error(const char* str, ...)
{
    if(log_out_fd == -1)
        return;
    int err = errno;
    va_list va;
    va_start(va, str);
    //dprintf(log_out_fd, "hexec: ");
    vdprintf(log_out_fd, str, va);
    va_end(va);
    errno = err;
}

void hexec_fatal(const char* str, ...)
{
    if(log_out_fd == -1)
        return;
    va_list va;
    va_start(va, str);
    //dprintf(log_out_fd, "hexec: ");
    vdprintf(log_out_fd, str, va);
    va_end(va);
    exit(EXIT_FAILURE);
}

