/***************************************************************************
 *   Copyright (C) 2008 by Alexander Block                                 *
 *   ablock@blocksoftware.net                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __HEXEC_ERROR_H__
#define __HEXEC_ERROR_H__

#include <stdio.h>

extern int log_out_fd;

int hexec_open_log_fd(const char* path);
void hexec_lock_log();
void hexec_unlock_log();

void hexec_log(const char* str, ...);
void hexec_warning(const char* str, ...);
void hexec_error(const char* str, ...);
void hexec_fatal(const char* str, ...);

#endif//__HEXEC_ERROR_H__

