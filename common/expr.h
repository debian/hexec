/***************************************************************************
 *   Copyright (C) 2008 by Alexander Block                                 *
 *   ablock@blocksoftware.net                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __HEXEC_EXPR_H__
#define __HEXEC_EXPR_H__

#include <stdbool.h>

#define HEXEC_EXPR_RESULT_MATCH 0
#define HEXEC_EXPR_RESULT_NOMATCH 1
#define HEXEC_EXPR_RESULT_ERROR 2

struct hexec_buf;
struct args_t;

enum hexec_expr_type
{
    hexec_expr_not,
    hexec_expr_and,
    hexec_expr_or,

    hexec_expr_path,
    hexec_expr_name,
    hexec_expr_contains,

    hexec_expr_action_print,
    hexec_expr_action_exec,
};

struct hexec_expr
{
    int type;
    int expr1;
    int expr2;
    int str; // can be path, name, contains, regex, ...
    bool case_insensitive;

    int argc;
    int argv;
};

struct hexec_expr_shm_header
{
    int root_expr;
};

///////////

int hexec_expr_eval(const char* path, struct args_t* args, struct args_t* env, struct hexec_buf* buf, int e, int* last_exec_status);

#endif//__HEXEC_EXPR_H__
