/***************************************************************************
 *   Copyright (C) 2008 by Alexander Block                                 *
 *   ablock@blocksoftware.net                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __HEXEC_SHM_H__
#define __HEXEC_SHM_H__

#include <stdbool.h>

struct hexec_buf;

struct hexec_shm
{
    bool owner;
    char* name;

    char* mmap;
    char* buf;
    int mmap_size;
    int buf_size;
};

int hexec_shm_create(struct hexec_shm* shm, const char* shm_name, struct hexec_buf* buf);
int hexec_shm_open(struct hexec_shm* buf,  const char* shm_name);
int hexec_shm_close(struct hexec_shm* buf);

int hexec_shm_to_buf(struct hexec_shm* shm, struct hexec_buf* buf);



#endif//__HEXEC_SHM_H__
