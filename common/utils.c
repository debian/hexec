/***************************************************************************
 *   Copyright (C) 2008 by Alexander Block                                 *
 *   ablock@blocksoftware.net                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "config.h"

#include "utils.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

char* hexec_xstrcat(char* s1, const char* s2, int* cur_len, int* cur_max_len)
{
    int s2_len = strlen(s2);
    int new_len = *cur_len + s2_len;
    bool do_realloc = false;
    while(new_len + 1 >= *cur_max_len)
    {
        *cur_max_len += 32;
        do_realloc = true;
    }
    if(do_realloc)
    {
        s1 = realloc(s1, *cur_max_len);
    }
    memcpy(s1 + *cur_len, s2, s2_len + 1);
    *cur_len += s2_len;
    return s1;
}

char* hexec_xstrcatch(char* s1, char ch, int* cur_len, int* cur_max_len)
{
    char tmp[2] = {ch, 0};
    return hexec_xstrcat(s1, tmp, cur_len, cur_max_len);
}
