/***************************************************************************
 *   Copyright (C) 2008 by Alexander Block                                 *
 *   ablock@blocksoftware.net                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
%{ 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parser.h"
#include "common/expr.h"
#include "common/args.h"

// better error reporting
#define YYDEBUG 1
#define YYERROR_VERBOSE 1
#define YYPARSE_PARAM yyparam_
#define PARSER ((struct hexec_parser*)YYPARSE_PARAM)
#define YYLEX_PARAM PARSER

int hexec_parser_lex(struct hexec_parser* p);

// bison requires that you supply this function
void yyerror(const char *msg)
{
      //printf("ERROR(PARSER): %s\n", msg);
}


%}

%union {
    int expr;
}

%token <expr> T_ACTION
%token <expr> T_CONSTANT

%token T_OB T_CB
%token T_UNARY_NOT
%token T_BINARY_AND
%token T_BINARY_OR

%type <expr> primary_expression unary_expression logical_and_expression logical_or_expression expression

%%
input
    : /* empty string */
    | expression
        { PARSER->result = 0; PARSER->shm_header->root_expr = $1; }
    ;

primary_expression
    : T_ACTION
    | T_CONSTANT
    | T_OB expression T_CB
        { $$ = $2; }
    ;

unary_expression
    : primary_expression
    | T_UNARY_NOT unary_expression
        { $$ = hexec_parser_build_not(PARSER, $2); }
    ;

logical_and_expression
    : unary_expression
    | logical_and_expression T_BINARY_AND unary_expression
        { $$ = hexec_parser_build_and(PARSER, $1, $3); }
    | logical_and_expression unary_expression
        { $$ = hexec_parser_build_and(PARSER, $1, $2); }
   ;

logical_or_expression
    : logical_and_expression
    | logical_or_expression T_BINARY_OR logical_and_expression
        { $$ = hexec_parser_build_or(PARSER, $1, $3); }
    ;

expression
    : logical_or_expression
    ;

%%

int hexec_parser_lex2(struct hexec_parser* p)
{
    if(p->cur_arg >= p->args->argc)
        return EOF;
    const char* a = p->args->argv[p->cur_arg++];
    if(!strcmp(a, "("))
        return T_OB;
    if(!strcmp(a, ")"))
        return T_CB;
    if(!strcmp(a, "!") || !strcmp(a, "-not"))
        return T_UNARY_NOT;
    if(!strcmp(a, "-a") || !strcmp(a, "-and"))
        return T_BINARY_AND;
    if(!strcmp(a, "-o") || !strcmp(a, "-or"))
        return T_BINARY_OR;

    if(!strcmp(a, "-print"))        return hexec_action_parse_print(p, &yylval.expr) ? T_ACTION : YYTERROR;
    if(!strcmp(a, "-path"))         return hexec_action_parse_fname_match(p, &yylval.expr, hexec_expr_path, false) ? T_ACTION : YYTERROR;
    if(!strcmp(a, "-ipath"))        return hexec_action_parse_fname_match(p, &yylval.expr, hexec_expr_path, true) ? T_ACTION : YYTERROR;
    if(!strcmp(a, "-name"))         return hexec_action_parse_fname_match(p, &yylval.expr, hexec_expr_name, false) ? T_ACTION : YYTERROR;
    if(!strcmp(a, "-iname"))        return hexec_action_parse_fname_match(p, &yylval.expr, hexec_expr_name, true) ? T_ACTION : YYTERROR;
    if(!strcmp(a, "-contains"))     return hexec_action_parse_fname_match(p, &yylval.expr, hexec_expr_contains, false) ? T_ACTION : YYTERROR;
    if(!strcmp(a, "-icontains"))    return hexec_action_parse_fname_match(p, &yylval.expr, hexec_expr_contains, true) ? T_ACTION : YYTERROR;
    if(!strcmp(a, "-exec"))         return hexec_action_parse_exec(p, &yylval.expr) ? T_ACTION : YYTERROR;
    if(!strcmp(a, "-sh"))           return hexec_action_parse_sh(p, &yylval.expr) ? T_ACTION : YYTERROR;

    return EOF;
}

int hexec_parser_lex(struct hexec_parser* p)
{
    int backup = p->cur_arg;
    int tok = hexec_parser_lex2(p);
    if(tok == EOF)
        p->cur_arg = backup;
    return tok;
}



