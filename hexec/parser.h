/***************************************************************************
 *   Copyright (C) 2008 by Alexander Block                                 *
 *   ablock@blocksoftware.net                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __PARSER_H__
#define __PARSER_H__

#include <stdbool.h>

struct hexec_buf;
struct hexec_expr_shm_header;
struct args_t;

struct hexec_parser
{
    struct hexec_buf* buf;
    struct args_t* args;
    int cur_arg;

    struct hexec_expr_shm_header* shm_header;
    int result;
};

void hexec_parser_init(struct hexec_parser* p, struct hexec_buf* buf, struct args_t* args, int first_arg);
int hexec_parser_parse(void* p);

int hexec_parser_build_not(struct hexec_parser* p, int expr);
int hexec_parser_build_and(struct hexec_parser* p, int left, int right);
int hexec_parser_build_or(struct hexec_parser* p, int left, int right);

bool hexec_action_parse_print(struct hexec_parser* p, int* action);
bool hexec_action_parse_fname_match(struct hexec_parser* p, int* action, int type, bool case_insensitive);
bool hexec_action_parse_exec(struct hexec_parser* p, int* action);
bool hexec_action_parse_sh(struct hexec_parser* p, int* action);



#endif//__PARSER_H__

