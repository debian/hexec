/***************************************************************************
 *   Copyright (C) 2008 by Alexander Block                                 *
 *   ablock@blocksoftware.net                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "config.h"

#include "common/expr.h"
#include "common/args.h"
#include "common/buf.h"
#include "common/error.h"
#include "common/locate.h"
#include "hexec/hexec.h"
#include "libhexec-hook.h"

#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <fnmatch.h>
#include <assert.h>
#include <unistd.h>
#include <sys/wait.h>
#include <libgen.h>

// found in expr-eval-exec.c
int eval_exec(const char* path, struct args_t* args, struct args_t* env, struct hexec_buf* buf, struct hexec_expr* e, int* last_exec_status);

static struct hexec_expr* to_expr(struct hexec_buf* buf, int p)
{
    return (struct hexec_expr*)(buf->buf + p);
}

static int eval_path(const char* path, struct args_t* args, struct args_t* env, struct hexec_buf* buf, struct hexec_expr* e)
{
    int flags = 0;
    if(e->case_insensitive)
        flags |= FNM_CASEFOLD;
    int ret = fnmatch(buf->buf + e->str, path, flags);
    if(ret == 0)
        return HEXEC_EXPR_RESULT_MATCH;
    if(ret == FNM_NOMATCH)
        return HEXEC_EXPR_RESULT_NOMATCH;
    return HEXEC_EXPR_RESULT_ERROR;
}

static int eval_name(const char* name, struct args_t* args, struct args_t* env, struct hexec_buf* buf, struct hexec_expr* e)
{
    char* name_copy = strdup(name);
    char* base_name = basename(name_copy);
//hexec_log("eval_name: n=%s, bn=%s, fn=%s\n", name, base_name, buf->buf + e->str);
    int flags = 0;
    if(e->case_insensitive)
        flags |= FNM_CASEFOLD;
    int ret = fnmatch(buf->buf + e->str, base_name, flags);
    free(name_copy);
    if(ret == 0)
        return HEXEC_EXPR_RESULT_MATCH;
    if(ret == FNM_NOMATCH)
        return HEXEC_EXPR_RESULT_NOMATCH;
    return HEXEC_EXPR_RESULT_ERROR;
}

static int eval_contains(const char* name, struct args_t* args, struct args_t* env, struct hexec_buf* buf, struct hexec_expr* e)
{
    if(e->case_insensitive)
    {
        char* name_copy = strdup(name);
        for(int i = 0; name_copy[i]; i++)
            name_copy[i] = tolower(name_copy[i]);
//hexec_log("icontains: %s, %s\n", name_copy, buf->buf + e->str);
        bool match = strstr(name_copy, buf->buf + e->str) != NULL; 
        free(name_copy);
        if(match)
            return HEXEC_EXPR_RESULT_MATCH;
        return HEXEC_EXPR_RESULT_NOMATCH;
    }
    else
    {
        if(strstr(name, buf->buf + e->str))
            return HEXEC_EXPR_RESULT_MATCH;
        return HEXEC_EXPR_RESULT_NOMATCH;
    }
}

static int eval_print(const char* path, struct args_t* args, struct args_t* env, struct hexec_buf* buf, struct hexec_expr* e)
{
//hexec_log("eval_print\n");
    //hexec_lock_log();
    //hexec_log("args = ");
    hexec_args_print(args);
    hexec_log("\n");
    //hexec_unlock_log();
    return HEXEC_EXPR_RESULT_NOMATCH;
}

int hexec_expr_eval(const char* path, struct args_t* args, struct args_t* env, struct hexec_buf* buf, int e, int* last_exec_status)
{
    struct hexec_expr* ex = to_expr(buf, e);
//hexec_log("eval: %d\n", ex->type);
    switch(ex->type)
    {
    case hexec_expr_not: {
        int res = hexec_expr_eval(path, args, env, buf, ex->expr1, last_exec_status);
        if(res == HEXEC_EXPR_RESULT_MATCH)
            return HEXEC_EXPR_RESULT_NOMATCH;
        if(res == HEXEC_EXPR_RESULT_NOMATCH)
            return HEXEC_EXPR_RESULT_MATCH;
        return HEXEC_EXPR_RESULT_ERROR;
    }
    case hexec_expr_and: {
        int leftRes = hexec_expr_eval(path, args, env, buf, ex->expr1, last_exec_status);
        if(leftRes == HEXEC_EXPR_RESULT_MATCH)
            return hexec_expr_eval(path, args, env, buf, ex->expr2, last_exec_status);
        return leftRes;
    }
    case hexec_expr_or: {
        int leftRes = hexec_expr_eval(path, args, env, buf, ex->expr1, last_exec_status);
        if(leftRes == HEXEC_EXPR_RESULT_MATCH)
            return HEXEC_EXPR_RESULT_MATCH;
        return hexec_expr_eval(path, args, env, buf, ex->expr2, last_exec_status);
    }
         
    case hexec_expr_action_print:
        return eval_print(path, args, env, buf, ex);
    case hexec_expr_path:
        return eval_path(path, args, env, buf, ex);
    case hexec_expr_name:
        return eval_name(path, args, env, buf, ex);
    case hexec_expr_contains:
        return eval_contains(path, args, env, buf, ex);
    case hexec_expr_action_exec:
        return eval_exec(path, args, env, buf, ex, last_exec_status);
    }
    hexec_log("unknown expr: %d\n", ex->type);
}
